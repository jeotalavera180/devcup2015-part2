﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace NpisDLL.Controllers
{
    public class BaseController<TRepository> : ApiController where TRepository : class, new()
    {
        TRepository repository;

        public TRepository Repository
        {
            get
            {
                if (repository == null)
                {
                    repository = new TRepository();
                }

                return repository;
            }
        }
    }
}
