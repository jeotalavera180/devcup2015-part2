﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Repositories
{
    public interface IViewable<TViewModel> where TViewModel : class, new()
    {
        IEnumerable<TViewModel> GetViewModels(string id = null);
    }
}
