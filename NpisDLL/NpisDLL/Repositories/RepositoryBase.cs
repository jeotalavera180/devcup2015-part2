﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpisDLL.Data;
using NpisDLL.Models;

namespace NpisDLL.Repositories
{
    public abstract class RepositoryBase
    {
        protected MDISEntities Context { get; private set; }

        public RepositoryBase()
        {
            this.Context = new MDISEntities();
        }
    }

    public class RepositoryBase<TViewModel> : RepositoryBase
       where TViewModel : class, new()
    {
        TViewModel viewModel;
        /**
        *  Instanciates the View Model to the repository
        * */
        protected TViewModel ViewModel
        {
            get
            {
                return this.viewModel;
            }
            set
            {
                this.viewModel = value;

                //while setting. Activate SetContext
                SetContext(value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        void SetContext(object target)
        {
            //loops through all properties of the object
            foreach (var propertyInfo in target.GetType().GetProperties())
            {
                if (typeof(ViewModelBase).IsAssignableFrom(propertyInfo.PropertyType))//The IsAssignableFrom method is used to check whether a Type is compatible with a given type.
                {//anything of type viewmodelBase will go here.
                    var child = propertyInfo.GetValue(target);//get value of that type

                    if (child != null)//if viewmodel is not empty
                    {
                        SetContext(child);//recursively setContext 
                    }
                }
                else if (propertyInfo.PropertyType == typeof(MDISEntities))
                {
                    propertyInfo.SetValue(target, this.Context);
                }
            }
        }

        public virtual TViewModel CreateModel()
        {
            return default(TViewModel);
        }
    }


    public abstract class RepositoryBase<TModel, TViewModel> : RepositoryBase
        where TModel : class, new()
        where TViewModel : class, new()
    {
        protected TModel Model;

        protected TViewModel ViewModel;
    }
}
