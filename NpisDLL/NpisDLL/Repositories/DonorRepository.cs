﻿using NpisDLL.Data;
using NpisDLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Repositories
{
    public class DonorRepository : RepositoryBase<DonorViewModel>
    {

        public IEnumerable<DonorViewModel> GetViewModels()
        {
            return DonorViewModel.getAll(Context);
        }

        public int EmailDonor(EmailDonorViewModel EmailDonor)
        {
            return EmailDonorViewModel.Email(EmailDonor);
        }
    }
}
