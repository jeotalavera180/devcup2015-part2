﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Repositories
{
    public interface IViewableSingle<TViewModel> where TViewModel : class, new()
    {
        TViewModel GetViewModel(string id);
    }
}
