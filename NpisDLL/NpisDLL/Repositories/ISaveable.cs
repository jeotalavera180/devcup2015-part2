﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Repositories
{
    public interface ISaveable<TViewModel>
        where TViewModel : class, new()
    {
        void Save(TViewModel viewModel);
    }

    public interface ISaveableReturn<TViewModel>
        where TViewModel : class, new()
    {
        String Save(TViewModel viewModel);
    }
}
