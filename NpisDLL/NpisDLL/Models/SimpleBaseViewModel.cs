﻿using NpisDLL.Data;
using NpisDLL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Models
{
    public abstract class SimpleBaseViewModel<TViewModel, TModel> : ViewModelBase<TModel>
        where TModel : class, new()
        where TViewModel : class, new()
    {
        public Guid ID { get; set; }

        public string Name { get; set; }

        public int Sort { get; set; }

        public TModel GetModel()
        {
            return Context.Set<TModel>().
                AsEnumerable().
                FirstOrDefault(
                simpleBase =>
                   this.GetModelMatch(simpleBase));
        }

        protected abstract Boolean GetModelMatch(TModel toMatch);

        public static TViewModel GetViewModel(TModel model)
        {
            return ViewModelTransformer.Transform<TModel, TViewModel>(model);
        }

        public static IEnumerable<TViewModel> GetViewModels(MDISEntities Context)
        {
            var models =
                Context.Set<TModel>().
                AsEnumerable().
                OrderBy(
                x => x.GetType().GetProperty("Sort").GetValue(x));

            var viewModels = new List<TViewModel>();

            models.ToList().ForEach(
                model =>
                {
                    viewModels.Add(GetViewModel(model));
                });

            return viewModels;
        }
    }
}
