﻿using NpisDLL.Data;
using NpisDLL.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Models
{
    public abstract class ViewModelBase
    {
        //public LoginViewModel Login { get; set; }

        protected MDISEntities context;

        public virtual MDISEntities Context
        {
            get
            {
                return context;
            }
            set
            {
                if (this.context != null || value == null)
                {
                    return;
                }

                this.context = value;

                SetContext(this);
            }
        }

        void SetContext(object target)
        {
            foreach (var propertyInfo in target.GetType().GetProperties())
            {
                if (typeof(ViewModelBase).IsAssignableFrom(propertyInfo.PropertyType))
                {
                    var child = propertyInfo.GetValue(target);

                    if (child != null)
                    {
                        SetContext(child);
                    }
                }
                else if (propertyInfo.PropertyType == typeof(MDISEntities))
                {
                    propertyInfo.SetValue(target, this.Context);
                }
            }
        }
    }

    public abstract class ViewModelBase<TEntity> : ViewModelBase
        where TEntity : class, new()
    {
        protected TEntity Entity;

        protected void CreateEntity()
        {
            Entity = new TEntity();
        }

        protected void SetAdded()
        {
            SetEntityID();

            Context.Set<TEntity>().Add(Entity);

            //InsertAuditTrail();
        }

        //void InsertAuditTrail()
        //{
        //    //if (Login == null)
        //    //{
        //    //    return;
        //    //}

        //    AuditTrail auditTrail = new AuditTrail()
        //    {
        //        ID = Guid.NewGuid(),
        //        Action = String.Format("Inserted {0}", typeof(TEntity)),
        //        ActionBy = "",
        //        Date = DateTime.Now,
        //        IsLogData = true,
        //        TableName = typeof(TEntity).Name
        //    };

        //    Context.AuditTrails.Add(auditTrail);
        //}

        void SetEntityID()
        {
            Entity.GetType().GetProperty("ID").SetValue(Entity, Guid.NewGuid());
        }

        protected void SetUpdated()
        {
            Context.Entry<TEntity>(Entity).State = System.Data.Entity.EntityState.Modified;
        }

        protected TEntity GetEntity(Func<TEntity, Boolean> predicate)
        {
            return Context.Set<TEntity>().FirstOrDefault(predicate);
        }

        protected TEntity Transform<TViewModel>(TViewModel viewModel)
        {
            return ViewModelTransformer.Transform<TViewModel, TEntity>(viewModel);
        }

        protected IEnumerable<TEntity> GetEntityMatches(Func<TEntity, Boolean> predicate)
        {
            return
                Context.Set<TEntity>().Where(predicate);
        }


        protected static TEntity GetEntity(Func<TEntity, Boolean> predicate, MDISEntities Context)
        {
            return
                Context.Set<TEntity>().FirstOrDefault(predicate);
        }

        protected static IEnumerable<TEntity> GetEntityMatches(Func<TEntity, Boolean> predicate, MDISEntities Context)
        {
            if (predicate == null)
            {
                return Context.Set<TEntity>();
            }
            else
            {
                return Context.Set<TEntity>().Where(predicate);
            }
        }
    }
}
