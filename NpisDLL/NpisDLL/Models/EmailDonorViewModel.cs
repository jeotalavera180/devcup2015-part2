﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Models
{
    public class EmailDonorViewModel
    {
        public string EmailFrom { get; set; }
        public string email { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }



        public static int Email(EmailDonorViewModel EmailDonor)
        {
            MailMessage message = BuildMessage(EmailDonor);
            
            var emailData = new EmailData
            {
                Message = message,
                SkipLock = false
            };

            Deliver(emailData);


            return 1;


        }

        public static MailMessage BuildMessage(EmailDonorViewModel EmailDonor)
        {
            var messageFrom = new MailAddress(EmailDonor.EmailFrom);
            var messageTo = new MailAddress(EmailDonor.email);
            string ccs = "";
            var mail = new MailMessage(messageFrom, messageTo)
            {
                Subject = EmailDonor.EmailSubject,
                Body = EmailDonor.EmailBody,
                IsBodyHtml = true
            };
            if (!String.IsNullOrEmpty(ccs))
            {
                ccs = ccs.Replace(EmailDonor.email, "");
                if (ccs.Contains(','))
                {
                    var ccEmails = ccs.Split(',');
                    foreach (var ccEmail in ccEmails)
                    {
                        if (!String.IsNullOrEmpty(ccEmail))
                        {
                            mail.CC.Add(ccEmail);
                        }
                    }
                }
                else if (!String.IsNullOrEmpty(ccs))
                {
                    mail.CC.Add(ccs);
                }
            }

            return mail;
        }

        public static void Deliver(EmailData emailData)
        {
            SendEmail(emailData);
        }

        public static void SendEmail(EmailData emailData)
        {
            var client = new SmtpClient();
            client.Send(emailData.Message);
            // _memoryStream.Close();
        }
    }

    #region Structs
    public struct EmailData
    {
        public MailMessage Message { get; set; }
        public bool SkipLock { get; set; }
    }
    #endregion


}
