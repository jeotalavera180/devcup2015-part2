﻿using NpisDLL.Data;
using NpisDLL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Models
{
    public class DonorViewModel : ViewModelBase<Donor>
    {
        public Guid Id { get; set; }
        public string BloodType { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public string ContactDetails { get; set; }
        public string SocialMediaLink { get; set; }
        public Nullable<int> VerifiedUser { get; set; }
        public string email { get; set; }
        public Nullable<DateTime> RecentDonateDate { get; set; }
        public Nullable<int> Age { get; set; }
        public string HealthDetails { get; set; }
        public byte[] Attachment { get; set; }
        public bool Eligible { get; set; }
        public List<DonationHistoryViewModel> DonationHistories { get; set; }


        /// <summary>
        /// Get All Donors from the Database
        /// </summary>
        /// <param name="Context"></param>
        /// <returns></returns>
        internal static IEnumerable<DonorViewModel> getAll(MDISEntities Context)
        {
            List<DonorViewModel> donors = new List<DonorViewModel>();

            var donorsContext = Context.Donors.ToList();

            donorsContext.ForEach(
                donor =>
                {
                    DonorViewModel d = ViewModelTransformer.Transform<Donor, DonorViewModel>(donor);
                    d.DonationHistories = DonationHistoryViewModel.GetViewModels(donor);
                    d.Eligible = CheckIfEligible(donor);
                    if (d.Eligible)
                    {
                        donors.Add(d);
                    }
                    else
                    {
                    }
                }
                );

            return donors;
        }

        private static bool CheckIfEligible(Donor donor)
        {
            var history = donor.DonationHistories.OrderBy(x => x.DateOfDonation).ToList();

            //if minor
            if (donor.Age < 18)
            {
                return false;
            }

            if (history.Count > 0)
            {
                //if latest date donated is not below 90 days from this day.
                if ((DateTime.Now - history.LastOrDefault().DateOfDonation).TotalDays <= 90)
                {
                    return false;
                }
            }
            List<DateTime> dates = new List<DateTime>();
            //add all date of donation to list
            //history.ForEach(x => dates.Add(x.DateOfDonation));

            DateTime oneYearAgo = DateTime.Now.AddDays(-365);

           //check how many times the user donated exactly 1 year ago till now
          int DonatedXTimesForThePastYear =  dates.Where(
              date=> 
              date.Date >= oneYearAgo.Date && DateTime.Now.Date <= date.Date
              ).ToList().Count;

            //unhealthy donating more than 3 times a year
          if (DonatedXTimesForThePastYear >= 3)
          {
              return false;
          }



            return true;
        }
    }
}
