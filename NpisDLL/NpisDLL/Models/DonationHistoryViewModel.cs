﻿using NpisDLL.Data;
using NpisDLL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NpisDLL.Models
{
    public class DonationHistoryViewModel : ViewModelBase<DonationHistory>
    {
        public Guid Id { get; set; }
        public Guid DonorID { get; set; }
        public DateTime DateOfDonation { get; set; }


        internal static List<DonationHistoryViewModel> GetViewModels(Donor donor)
        {
            List<DonationHistoryViewModel> historyVM = new List<DonationHistoryViewModel>();
            using (var Context = new MDISEntities())
            {
                Context.DonationHistories.Where(x => donor.Id == x.DonorID).OrderBy(x => x.DateOfDonation).ToList().ForEach(

                    history =>
                    {
                        DonationHistoryViewModel d = ViewModelTransformer.Transform<DonationHistory, DonationHistoryViewModel>(history);
                        historyVM.Add(d);
                    }
                    );

                return historyVM;

            }
        }
    }
}
