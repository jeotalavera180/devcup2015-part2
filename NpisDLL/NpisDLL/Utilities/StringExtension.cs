﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL
{
    internal static class StringExtension
    {
        public static Boolean IsUndefined(this string value)
        {
            return
                value == null ||
                String.IsNullOrWhiteSpace(value) ||
                value == "undefined";
        }
    }
}
