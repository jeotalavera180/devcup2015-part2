﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL
{
    internal static class GuidExtension
    {
        public static Boolean IsNull(this Guid idValue)
        {
            return idValue == Guid.Empty;
        }
    }
}
