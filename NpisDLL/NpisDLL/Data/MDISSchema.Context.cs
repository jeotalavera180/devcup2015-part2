﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NpisDLL.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class MDISEntities : DbContext
    {
        public MDISEntities()
            : base("name=MDISEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<DonationHistory> DonationHistories { get; set; }
        public DbSet<Donor> Donors { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
    }
}
