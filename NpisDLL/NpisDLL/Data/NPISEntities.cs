﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NpisDLL.Data
{
    public partial class MDISEntities : DbContext
    {
        public override int SaveChanges()
        {
            try
            {
                //var manager = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager;

                //foreach (ObjectStateEntry entry in manager.GetObjectStateEntries(EntityState.Added | EntityState.Modified))
                //{
                //    var properties = entry.GetModifiedProperties();

                //    if (!(properties.Count() == 1 && properties.First() == "ColumnA"))
                //    {
                //        //modify timestamp here
                //    }
                //}

                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                    ); // Add the original exception as the innerException
            }
        }



    }
}
