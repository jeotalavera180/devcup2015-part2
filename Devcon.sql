USE [master]
GO
/****** Object:  Database [Devcup]    Script Date: 08/16/2015 12:51:39 ******/
CREATE DATABASE [Devcup] ON  PRIMARY 
( NAME = N'Devcup', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Devcup.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Devcup_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Devcup_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Devcup] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Devcup].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Devcup] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Devcup] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Devcup] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Devcup] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Devcup] SET ARITHABORT OFF
GO
ALTER DATABASE [Devcup] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Devcup] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Devcup] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Devcup] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Devcup] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Devcup] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Devcup] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Devcup] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Devcup] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Devcup] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Devcup] SET  DISABLE_BROKER
GO
ALTER DATABASE [Devcup] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Devcup] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Devcup] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Devcup] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Devcup] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Devcup] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Devcup] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Devcup] SET  READ_WRITE
GO
ALTER DATABASE [Devcup] SET RECOVERY FULL
GO
ALTER DATABASE [Devcup] SET  MULTI_USER
GO
ALTER DATABASE [Devcup] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Devcup] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Devcup', N'ON'
GO
USE [Devcup]
GO
/****** Object:  Table [dbo].[Donor]    Script Date: 08/16/2015 12:51:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Donor](
	[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[BloodType] [nchar](10) NOT NULL,
	[Location] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[ContactDetails] [varchar](50) NULL,
	[SocialMediaLink] [varchar](50) NULL,
	[VerifiedUser] [int] NULL,
	[email] [varchar](50) NULL,
	[RecentDonateDate] [datetime] NULL,
	[Age] [int] NULL,
	[HealthDetails] [text] NULL,
	[Attachment] [varbinary](2000) NULL,
 CONSTRAINT [PK_Donor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'4126c2f3-44e2-405c-ac3d-00f0073997bc', N'B         ', N'Teacher''s Village, Quezon City', N'Mina Deocareza', N'09271629368', N'twitter.com/lrtqueen', 1, N'qcrublico@up.edu.ph', NULL, 28, N'generally in good health condition', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'ab332ec1-3cba-4b26-ba7f-06eed53674c4', N'AB+       ', N'Talon IV, Las Pinas', N'Florencio Rublico Sr.', N'09351863892', N'facebook.com/bobbysr', 1, N'qcrublico@up.edu.ph', NULL, 36, N'Lung disease survivor', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'aac95bc6-4b74-44de-8397-0bc29aba9308', N'B         ', N'Mandaluyong', N'Yves Libertine', N'09338888888', N'facebook.com/yves.libertine', 1, N'yves.libertine@gmail.com', NULL, 24, N'Sexy, fit, fat-free', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'2bc19741-6501-40d9-a9ef-11ec74b88291', N'B+        ', N'RFC, Las Pinas', N'Mary Jane Tijada', N'09357614797', N'facebook.com/jhane', 1, N'qcrublico@up.edu.ph', NULL, 21, N'Social drinker, generally fit', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'c596a036-4c5c-47ef-a796-193f121c167c', N'AB        ', N'Las Pinas City', N'Florencio Golez', N'09125643870', N'facebook.com/florenciosr', 1, N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'ad3237f8-accb-4892-b2d8-2a8f49f75e1a', N'A         ', N'Katipunan Ave., Quezon City', N'Jayson Nalus', N'09215437965', N'facebook.com/jays.nalus', 1, N'jjnalus@ittc.up.edu.ph', NULL, 25, N'Light smoker', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'92f11bc4-b760-4d8e-b6ba-3ca0c5684b4b', N'O+        ', N'Deparo, North Caloocan City', N'Mark Jezhroe Rublico', N'09161946030', N'facebook.com/hazardeyes', 0, N'querub.03@gmail.com', NULL, 22, N'Fit and active, good diet', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'ba330f81-c60f-4658-a07d-4398bd54095c', N'A+        ', N'BF Homes, Las Pinas', N'Patrick Maling', N'09331527936', N'facebook.com/pat.maling', 0, N'xyzabc.querub@gmail.com', NULL, 28, N'Donated blood twice in my whole lifetime', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'b8e7f5b2-f367-45a7-b2de-5b872c750f47', N'B+        ', N'Mandaluyong', N'johnny', N'222222', N'facebook.com', 0, N'boom@gmail.com', NULL, 50, N'recently got cancer', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'd9c45b56-5f38-4053-bb04-5ca3984084e2', N'A         ', N'Bagumbong, North Caloocan', N'Angel Lorgonio', N'09152374957', N'facebook.com/larclorgorgonio', 1, N'xyzabc.querub@gmail.com', NULL, 26, N'I lose and gain weight easily at will', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'a6054242-1bb1-4f90-a62a-75b19654b2c0', N'B         ', N'San Juan City', N'Patricia Maravilla', N'09361638456', N'facebook.com/patish', 1, N'querub.03@gmail.com', NULL, 23, N'Vegetarian diet', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'50f7cc88-6eb8-4f9c-b409-83ab5ef20420', N'AB+       ', N'EDSA Shangri-la', N'Nick', N'09326518965', N'twitter.com/nick2001', 1, N'xyzabc.querub@gmail.com', NULL, 33, N'Fit and healthy lifestyle as always', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'c0753061-ec23-4796-8547-865c0807c369', N'AB        ', N'Maginhawa st., Quezon City', N'Brian Gelus', N'09257997658', N'twitter.com/iambrian', 0, N'jjnalus@ittc.up.edu.ph', NULL, 32, N'Non-smoking, non-alcoholic', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'222f1759-65bb-4079-908c-8da5824108c0', N'B         ', N'Makati', N'jayson aranilla', N'0292929222', N'youtube.com/jayson', 1, N'jays.jay@gmail.com', NULL, 22, N'Always have flu', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'140b1dcb-df6c-4c1c-af66-98b83b9995c5', N'B+        ', N'Legarda, Manila', N'Herbert Lago', N'09237614538', N'facebook.com/', 0, N'qcrublico@up.edu.ph', NULL, 27, N'Post-op trans', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'73101db9-e375-48dd-b48a-a16e05af291e', N'O         ', N'Ortigas Center', N'Alexander Hans Collado', N'09159874572', N'facebook.com/', 1, N'qcrublico@up.edu.ph', NULL, 19, N'I workout and jog regularly.', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'67d9ab32-228d-4025-b300-bf7ad8916649', N'A         ', N'Pureza, Manila', N'Katleen Chavez', N'09335441155', N'facebook.com/prettykat', 1, N'yves.libertine@gmail.com', NULL, 23, N'Never been confined in a hospital my whole life', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'55bb3bef-759b-43c4-9ff5-d9084c712a35', N'A+        ', N'Makati', N'johnny debit', N'0929292', NULL, 0, N'jeo.talavera@gmail.com', NULL, 20, N'I have been jail', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'523f9c87-a2a7-4245-851d-ea87884a4191', N'O+        ', N'Old Sta.Mesa, Manila', N'Raymond Gatchalian', N'09321638725', N'facebook.com/moxkie', 0, N'querub.03@gmail.com', NULL, 23, N'Just took anti-hepa vaccine shots a month ago', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'd1620019-c703-4222-b52a-f3ca44220c8f', N'A         ', N'215 General Luna street Malabon City', N'John Walter Talavera', N'09179667084', N'facebook.com', 1, N'jeo.talavera@gmail.com', NULL, 18, N'Healthy since Birth', NULL)
INSERT [dbo].[Donor] ([Id], [BloodType], [Location], [Name], [ContactDetails], [SocialMediaLink], [VerifiedUser], [email], [RecentDonateDate], [Age], [HealthDetails], [Attachment]) VALUES (N'4009b502-8b69-4ebb-a355-f947faa9056d', N'O         ', N'BF Homes, Paranaque', N'Paolo Chavez', N'09283267548', N'twitter.com/pcpcpc', 1, N'querub.03@gmail.com', NULL, 36, N'light smoker, light drinker', NULL)
/****** Object:  Table [dbo].[DonationHistory]    Script Date: 08/16/2015 12:51:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DonationHistory](
	[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[DonorID] [uniqueidentifier] NOT NULL,
	[DateOfDonation] [datetime] NOT NULL,
 CONSTRAINT [PK_DonationHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'a89f12d5-c029-4ba4-9b62-1814d2c3439b', N'b8e7f5b2-f367-45a7-b2de-5b872c750f47', CAST(0x00009FDE00000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'd620e3cf-0d7f-42b5-a106-3762fd9e7b8e', N'b8e7f5b2-f367-45a7-b2de-5b872c750f47', CAST(0x0000A03900000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'cf15ff3b-d793-4021-8072-3d19bd55ff67', N'b8e7f5b2-f367-45a7-b2de-5b872c750f47', CAST(0x0000A0B300000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'8a73b8b0-18d9-4a42-a971-516852f94d69', N'd1620019-c703-4222-b52a-f3ca44220c8f', CAST(0x0000A39700000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'931f48b9-a45b-495d-8274-56f80b5743e1', N'd1620019-c703-4222-b52a-f3ca44220c8f', CAST(0x0000A48A00000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'd393ade3-c546-4f39-9cbd-766ed886e6ef', N'd1620019-c703-4222-b52a-f3ca44220c8f', CAST(0x0000A31D00000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'56120d0e-cf5e-40a9-8d1c-824833803ebe', N'd1620019-c703-4222-b52a-f3ca44220c8f', CAST(0x0000A40700000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'7b0c77f5-a404-48a4-a919-89e4a4a0f57c', N'b8e7f5b2-f367-45a7-b2de-5b872c750f47', CAST(0x0000A12D00000000 AS DateTime))
INSERT [dbo].[DonationHistory] ([Id], [DonorID], [DateOfDonation]) VALUES (N'857d1300-7091-4190-b5a9-f70feea0393c', N'd1620019-c703-4222-b52a-f3ca44220c8f', CAST(0x0000A2C300000000 AS DateTime))
/****** Object:  Default [DF_Donor_id]    Script Date: 08/16/2015 12:51:40 ******/
ALTER TABLE [dbo].[Donor] ADD  CONSTRAINT [DF_Donor_id]  DEFAULT (newid()) FOR [Id]
GO
/****** Object:  Default [DF_DonationHistory_Id]    Script Date: 08/16/2015 12:51:40 ******/
ALTER TABLE [dbo].[DonationHistory] ADD  CONSTRAINT [DF_DonationHistory_Id]  DEFAULT (newid()) FOR [Id]
GO
/****** Object:  ForeignKey [FK_DonationHistory_Donor]    Script Date: 08/16/2015 12:51:40 ******/
ALTER TABLE [dbo].[DonationHistory]  WITH CHECK ADD  CONSTRAINT [FK_DonationHistory_Donor] FOREIGN KEY([DonorID])
REFERENCES [dbo].[Donor] ([Id])
GO
ALTER TABLE [dbo].[DonationHistory] CHECK CONSTRAINT [FK_DonationHistory_Donor]
GO
