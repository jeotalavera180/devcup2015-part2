﻿using NpisDLL.Controllers;
using NpisDLL.Data;
using NpisDLL.Models;
using NpisDLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NapolcomIS.Controllers
{
    public class DonorController : BaseController<DonorRepository>
    {
        public IEnumerable<DonorViewModel> GetAll()
        {
            return Repository.GetViewModels();
        }

        public IHttpActionResult EmailDonor([FromBody]EmailDonorViewModel pciModel)
        {
            var id = Repository.EmailDonor(pciModel);

            return Ok(
                new
                {
                    ID = id
                });
        }
       
    }
}
