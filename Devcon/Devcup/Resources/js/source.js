// Collapse Panel
$('.close-link').click(function () {
    var content = $(this).closest('div.x_panel');
    content.remove();
});

// Collapse ibox function
$('.collapse-link').click(function () {
    var x_panel = $(this).closest('div.x_panel');
    var button = $(this).find('i');
    var content = x_panel.find('div.x_content');
    content.slideToggle(200);
    (x_panel.hasClass('fixed_height_390') ? x_panel.toggleClass('').toggleClass('fixed_height_390') : '');
    (x_panel.hasClass('fixed_height_320') ? x_panel.toggleClass('').toggleClass('fixed_height_320') : '');
    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    setTimeout(function () {
        x_panel.resize();
    }, 50);
});


// Type of Dispo NAB
$(document).ready(function() {
    $('#othersTD-nab').hide();

     $('#type-dispo-nab').change(function () {
        if ($('#type-dispo-nab option:selected').text() == "Others"){
            $('#othersTD-nab').show();
        }
         else {
            $('#othersTD-nab').hide();
         }
    });
});

$(document).ready(function() {
    $('#modifiedTD-nab').hide();

     $('#type-dispo-nab').change(function () {
        if ($('#type-dispo-nab option:selected').text() == "Modified"){
            $('#modifiedTD-nab').show();
        }
         else {
            $('#modifiedTD-nab').hide();
         }
    });
});

// Type of Dis
$(document).ready(function() {
    $('#othersTD').hide();

     $('#type-dispo').change(function () {
        if ($('#type-dispo option:selected').text() == "Others"){
            $('#othersTD').show();
        }
         else {
            $('#othersTD').hide();
         }
    });
});

$(document).ready(function() {
    $('#modifiedTD').hide();

     $('#type-dispo').change(function () {
        if ($('#type-dispo option:selected').text() == "Modified"){
            $('#modifiedTD').show();
        }
         else {
            $('#modifiedTD').hide();
         }
    });
});

// In Case of Motion
$(document).ready(function() {
    $('#preventive-suspension').hide();

     $('#in-motion').change(function () {
        if ($('#in-motion option:selected').text() == "In Case of Motion for Preventive Suspension"){
            $('#preventive-suspension').show();
        }
         else {
            $('#preventive-suspension').hide();
         }
    });
});

$(document).ready(function() {
    $('#transfer-venue').hide();

     $('#in-motion').change(function () {
        if ($('#in-motion option:selected').text() == "In Case of Motion for Transfer of Venue"){
            $('#transfer-venue').show();
        }
         else {
            $('#transfer-venue').hide();
         }
    });
});

$(document).ready(function() {
    $('#preventive-inhibition').hide();

     $('#in-motion').change(function () {
        if ($('#in-motion option:selected').text() == "In Case of Motion for Preventive for Inhibition"){
            $('#preventive-inhibition').show();
        }
         else {
            $('#preventive-inhibition').hide();
         }
    });
});

// DATE DRAFT MR
$(document).ready(function() {
    $('#OCPNP-MR').hide();

     $('#date-draftMR').change(function () {
        if ($('#date-draftMR option:selected').text() == "OCPNP"){
            $('#OCPNP-MR').show();
        }
         else {
              $('#OCPNP-MR').hide();
         }
    });
});

$(document).ready(function() {
    $('#OCCDG-MR').hide();

     $('#date-draftMR').change(function () {
        if ($('#date-draftMR option:selected').text() == "OCCDG"){
            $('#OCCDG-MR').show();
        }
         else {
              $('#OCCDG-MR').hide();
         }
    });
});

$(document).ready(function() {
    $('#OCASU-MR').hide();

     $('#date-draftMR').change(function () {
        if ($('#date-draftMR option:selected').text() == "OCASU"){
            $('#OCASU-MR').show();
        }
         else {
              $('#OCASU-MR').hide();
         }
    });
});

$(document).ready(function() {
    $('#OCLTP-MR').hide();

     $('#date-draftMR').change(function () {
        if ($('#date-draftMR option:selected').text() == "OCLTP"){
            $('#OCLTP-MR').show();
        }
         else {
              $('#OCLTP-MR').hide();
         }
    });
});

$(document).ready(function() {
    $('#OVCEO-MR').hide();

     $('#date-draftMR').change(function () {
        if ($('#date-draftMR option:selected').text() == "OVCEO"){
            $('#OVCEO-MR').show();
        }
         else {
              $('#OVCEO-MR').hide();
         }
    });
});

$(document).ready(function() {
    $('#OSILG-MR').hide();

     $('#date-draftMR').change(function () {
        if ($('#date-draftMR option:selected').text() == "OC/OSILG"){
            $('#OSILG-MR').show();
        }
         else {
              $('#OSILG-MR').hide();
         }
    });
});

// DATE DRAFT
$(document).ready(function() {
    $('#OCPNP').hide();

     $('#date-draft').change(function () {
        if ($('#date-draft option:selected').text() == "OCPNP"){
            $('#OCPNP').show();
        }
         else {
              $('#OCPNP').hide();
         }
    });
});

$(document).ready(function() {
    $('#OCCDG').hide();

     $('#date-draft').change(function () {
        if ($('#date-draft option:selected').text() == "OCCDG"){
            $('#OCCDG').show();
        }
         else {
              $('#OCCDG').hide();
         }
    });
});

$(document).ready(function() {
    $('#OCASU').hide();

     $('#date-draft').change(function () {
        if ($('#date-draft option:selected').text() == "OCASU"){
            $('#OCASU').show();
        }
         else {
              $('#OCASU').hide();
         }
    });
});

$(document).ready(function() {
    $('#OCLTP').hide();

     $('#date-draft').change(function () {
        if ($('#date-draft option:selected').text() == "OCLTP"){
            $('#OCLTP').show();
        }
         else {
              $('#OCLTP').hide();
         }
    });
});

$(document).ready(function() {
    $('#OVCEO').hide();

     $('#date-draft').change(function () {
        if ($('#date-draft option:selected').text() == "OVCEO"){
            $('#OVCEO').show();
        }
         else {
              $('#OVCEO').hide();
         }
    });
});

$(document).ready(function() {
    $('#OSILG').hide();

     $('#date-draft').change(function () {
        if ($('#date-draft option:selected').text() == "OC/OSILG"){
            $('#OSILG').show();
        }
         else {
              $('#OSILG').hide();
         }
    });
});

// DISMISSAL PROCEEDINGS
$(document).ready(function() {
    $('#CR').hide();

     $('#dismissal-pro').change(function () {
        if ($('#dismissal-pro option:selected').text() == "Date Case Folder Received at Central Records"){
            $('#CR').show();
        }
         else {
              $('#CR').hide();
         }
    });
});

$(document).ready(function() {
    $('#LAS').hide();

     $('#dismissal-pro').change(function () {
        if ($('#dismissal-pro option:selected').text() == "Date Case Folder Received at LAS"){
            $('#LAS').show();
        }
         else {
              $('#LAS').hide();
         }
    });
});

$(document).ready(function() {
    $('#EO').hide();

     $('#dismissal-pro').change(function () {
        if ($('#dismissal-pro option:selected').text() == "Date Case Folder Received by the Evaluating Officer"){
            $('#EO').show();
        }
         else {
              $('#EO').hide();
         }
    });
});


//COMP SOURCE

$(document).ready(function() {
    $('#referral').hide();

     $('#com-source').change(function () {
        if ($('#com-source option:selected').text() == "Referral"){
            $('#referral').show();
        }
         else {
              $('#referral').hide();
         }
    });
});

$(document).ready(function() {
    $('#source-others').hide();

     $('#com-source').change(function () {
        if ($('#com-source option:selected').text() == "Others"){
            $('#source-others').show();
        }
         else {
              $('#source-others').hide();
         }
    });
});


// Hide all but one method div (since all are shown in case the user has JS disabled)
function hideReferrals(){
  $('.radio-ref').hide();
}

function showReferrals() {
  $('.radio-ref').show();
}



// Attach to the radio buttons when they change
$('#rad-walk-in, #rad-letter, #rad-motu, #rad-ref, #rad-email, #rad-phone, #rad-social').on('change', function () {
   // Make sure that this change is because a radio button has been checked
  if (!this.checked) return

  // Check which radio button has changed
  if (this.id == 'rad-ref') {
    showReferrals();
  } else {
    hideReferrals();
  }
});
hideReferrals();


// REFFERED TO OTHERS

function hideReferralsPnp(){
  $('.radio-child-pnp').hide();
}

function showReferralsPnp() {
  $('.radio-child-pnp').show();
}

function hideReferralsPleb(){
  $('.radio-child-pleb').hide();
}

function showReferralsPleb() {
  $('.radio-child-pleb').show();
}
function hideReferralsOthers(){
  $('.radio-child-others').hide();
}

function showReferralsOthers() {
  $('.radio-child-others').show();
}

// Attach to the radio buttons when they change
$('#rad-pnp-date, #rad-pleb-date, #rad-others-date').on('change', function () {
   // Make sure that this change is because a radio button has been checked
  if (!this.checked) return

  // Check which radio button has changed
  if (this.id == 'rad-pnp-date') {
    showReferralsPnp();
  } else {
    hideReferralsPnp();
  }
  if (this.id == 'rad-pleb-date') {
    showReferralsPleb();
  } else {
    hideReferralsPleb();
  }
  if (this.id == 'rad-others-date') {
    showReferralsOthers();
  } else {
    hideReferralsOthers();
  }
});
hideReferrals();

// Type of Disposition

function hideReferralsSd(){
  $('.radio-child-sd').hide();
}

function showReferralsSd() {
  $('.radio-child-sd').show();
}

function hideReferralsDropping(){
  $('.radio-child-dropping').hide();
}

function showReferralsDropping() {
  $('.radio-child-dropping').show();
}

// Attach to the radio buttons when they change
$('#rad-sd-date, #rad-dropping-date').on('change', function () {
   // Make sure that this change is because a radio button has been checked
  if (!this.checked) return

  // Check which radio button has changed
  if (this.id == 'rad-sd-date') {
    showReferralsPnp();
  } else {
    hideReferralsPnp();
  }
  if (this.id == 'rad-dropping-date') {
    showReferralsPleb();
  } else {
    hideReferralsPleb();
  }
});
hideReferrals();
