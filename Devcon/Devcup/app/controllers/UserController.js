﻿(
function () {

    var UserController = function (
        $location, $window,
        UserService, NotificationService,
        SharedDataService) {

        var vm = this;

        vm.User = {};

        vm.login = function () {

            UserService.login(vm.User).$promise.then(
                function (value) {

                    vm.User = value;

                    //put it on cookies provided by the service
                    SharedDataService.set("UserID", vm.User.ID);

                    SharedDataService.set("Username", vm.User.Username);

                    SharedDataService.set("RegionID", vm.User.Region.ID);

                    //redirect to complaints route
                    $location.url('/Complaints');

                    //reload the page
                    $window.location.reload();
                },

                function (error) {

                    NotificationService.error("Login error");

                });
        }

        vm.showError = function () {

            return vm.User.ErrorMessage != null;
        }

        vm.getRegions = function () {

            UserService.getRegions().$promise.then(
                function (value) {

                    vm.Regions = value;
                },

                function (error) {

                });
        }

        vm.getActiveUser = function () {

            UserService.getActiveUser().$promise.then(
                function (value) {

                    vm.User = value;
                },

                function (error) {

                });
        }

        vm.save = function () {

            vm.User.Roles = [];

            vm.User.Roles.push(vm.User.Role);

            UserService.save(vm.User).$promise.then(
                function (value) {

                    NotificationService.success("successfully saved user");
                },

                function (error) {

                });
        }
    };

    app.controller('UserController', UserController);
}
());