﻿(
function () {

    var DonorController = function (
        $location,
        NotificationService,
        DonorService,
        SharedDataService) {

        vm = this;

        vm.Donors = [];

        vm.getDonors = function () {

            DonorService.getDonors().$promise.then(
                function (value) {

                    vm.Donors = value;
                },

                function (error) {

                    NotificationService.error("Error in finding Donors");
                });

        };
        vm.DonorAttachment = {};
        vm.onFileSelect = function ($files) {


            if (!$files || $files.length == 0) {
                vm.hasAttachment = false;
                return;
            }

            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];

                if (file.size > 5000000) {
                    vm.hasAttachment = false;
                    NotificationService.error("Image size limit is 5 mb");

                    //vm.DonorAttachmentFileName = [];

                    //vm.DonorAttachmentFileType = [];

                    //vm.DonorAttachment = [];

                    return;
                }

                vm.hasAttachment = true;

                vm.DonorAttachmentFileName = file.name;

                vm.DonorAttachmentFileType = file.type;

                vm.DonorAttachment = file;
                break;
            }
        }

        vm.AddToDonorList = function () {

            //var d = new Date(vm.DonorDonationDate);
            //var parsedDate = d.formatDate('MM/dd/yyyy', d);
            var z = false;
            if (vm.DonorAge > 18) {
                z = true;
            }
            newDonor = {
                Name: vm.DonorName,
                Age: vm.DonorAge,
                BloodType: vm.DonorType,
                Location: vm.DonorLocation,
                ContactDetails: vm.DonorContact,
                email: vm.DonorEmail,
                RecentDonateDate: vm.DonorDonationDate,
                VerifiedUser: vm.DonorAttachment.lastModified ? 1 : 0,
                HealthDetails: vm.DonorDetails,
                Attachment: vm.DonorAttachment,
                AttachmentFileName: vm.DonorAttachmentFileName,
                AttachmentFileType: vm.DonorAttachmentFileType,
                Eligible:z
            };

            vm.Donors.push(newDonor);

            vm.DonorName = '';
            vm.DonorAge = '';
            vm.DonorType = '';
            vm.DonorLocation = '';
            vm.DonorContact = '';
            vm.DonorEmail = '';
            vm.DonorDonationDate = '';
            vm.DonorDetails = '';
            vm.DonorAttachment = '';
            vm.DonorAttachmentFileName = '';
            vm.DonorAttachmentFileType = '';

            //then send to server side to save.
            vm.AddDonorToServer(newDonor);
        }

        vm.AddDonorToServer = function (newDonor) {

            //call server
        };

        vm.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        vm.formats = ['MM/dd/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        vm.format = vm.formats[0];

        vm.open = function ($event, opened, recipient) {

            $event.preventDefault();
            $event.stopPropagation();

            vm[opened] = true;
            if (recipient != null) {
                recipient.temp = 'true';
            }
        };

        vm.TransferDataToModal = function (Histories) {

            vm.DonationHistories = Histories;

        };

        vm.TransferDataToEmailModal = function (donors) {
            vm.EmailDonor = donors;
            vm.EmailDonor.EmailSubject = "I am in need of a blood donor";
            vm.EmailDonor.EmailBody = "ako ay nangangailangan ng donor at isa ka sa mga eligible na donor sa website. pwede mo ba ako matulungan? e2 ang contact number ko XXXXX";

        }
        
        vm.Email = function(){
            DonorService.EmailDonor(vm.EmailDonor).$promise.then(
                function (value) {

                    NotificationService.success("Email Successful");
                },

                function (error) {

                    NotificationService.error("There has been a problem in Sending Email");
                });
        }


    };

    app.controller('DonorController', DonorController);
}
());
