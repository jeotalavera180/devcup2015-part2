﻿(
function () {

    var StatusService = function ($resource) {

        return {
            getAppellateStatus: function(){
                return $resource('/api/status/getappellatestatus/').query();
            },
            getStatus: function () {

                return $resource('/api/status/getx/').query();
            },
            setStatus: function (id,status) {

                return $resource('/api/status/get/' + id+'/'+status).get();
            },

        }
    }

    app.factory('StatusService', ['$resource', StatusService]);
}
());