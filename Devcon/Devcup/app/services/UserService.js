﻿(
function () {

    var UserService = function ($resource) {

        return {

            getRegions: function () {

                return $resource('/api/region/get').query();
            },

            save: function (user) {

                return $resource('/api/user/post').save(user);
            },

            login: function (user) {

                return $resource('/api/login/login').save(user);
            },

            getUsers: function () {

                return $resource('/api/user/GetUsers').query();
            },

            getActiveUser: function (id) {

                return $resoures('/api/user/Get/' + id).get();

            },

            getUserPrivileges: function (userID) {

                return $resource('/api/user/GetUserPrivileges/' + userID).query();
            }
        }
    };

    app.factory('UserService', ['$resource', UserService]);
}
());