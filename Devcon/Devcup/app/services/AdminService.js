﻿(
function () {

    var AdminService = function ($resource) {

        return {

            getRoles: function () {

                return $resource('/api/role/get').query();
            },

            getPrivilegeAreas: function () {

                return $resource('/api/privilegearea/get').query();
            },

            getPrivileges: function () {

                return $resource('/api/privilege/get').query();
            },

            getSelectedRolePrivileges: function () {

                return $resource('/api/roleprivilege/get').query();
            },

            save: function (roleProvilege) {

                return $resource('/api/roleprivilege/post').save(roleProvilege);
            }

        };

    }

    app.factory('AdminService', ['$resource', AdminService]);
}
());