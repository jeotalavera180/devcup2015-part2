﻿(
function () {

    var NotificationService = function () {

        return {
            success: function (text) {
                toastr.success(text, "Success");
            },
            error: function (text) {
                toastr.error(text, "Error");
            }
        };
    };

    app.factory('NotificationService', NotificationService);

}
());