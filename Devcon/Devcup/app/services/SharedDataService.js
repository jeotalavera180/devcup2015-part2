﻿(
function () {

    var SharedDataService = function ($cookies) {

        var loginData =
        {
            ID: '',
            Name: '',
            Role: ''
        }

        return {
            //get and set cookies
            get: function (key) {

                return $cookies.get(key);
            },

            set: function (key, value) {

                $cookies.put(key, value);
            },
            //remove cookie by name
            remove: function (key) {

                $cookies.remove(key);
            }
        };
    };

    app.factory('SharedDataService', ['$cookies',SharedDataService]);
}
());