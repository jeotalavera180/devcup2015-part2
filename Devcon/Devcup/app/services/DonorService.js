﻿(
function () {

    var DonorService = function ($resource) {

        return {

            getDonors: function () {

                return $resource('/api/donor/getAll/').query();
            }
            ,
            EmailDonor: function (EmailDonor) {
                return $resource('api/donor/EmailDonor/').save(EmailDonor);
            }

           
        }
    }

    app.factory('DonorService', ['$resource', DonorService]);
}
());