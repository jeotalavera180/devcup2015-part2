﻿var app = angular.module('DonorModule', ['smart-table', 'ngCookies', 'ngFileUpload', 'ngRoute', 'ui.bootstrap', 'ngResource', 'ngSanitize']);

app.config(function ($routeProvider) {

    $routeProvider
        .when('/Donors',
        {
            templateUrl: 'app/views/Donors.html',
            controller: 'DonorController as vm'
        })
        .when('/Dashboard',
        {
            templateUrl: 'app/views/Dashboard.html',
            controller: 'DonorController as vm'
        })
        .when('/Search',
        {
            templateUrl: 'app/views/Search.html',
            controller: 'DonorController as vm'
        })

        .otherwise({ redirectTo: '/Dashboard' });
});
